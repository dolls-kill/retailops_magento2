# RetailOps integration for Magento 2

The RetailOps Magento2 extension is an implementation of the protocol specified by the [RetailOps Channel SDK](https://github.com/gudtech/retailops-sdk).

Certification of a Magento 2 implementation of the RetailOps Channel SDK protocol (per host) is required prior to activation of a given sales channel.
For details, please see https://github.com/gudtech/retailops-sdk/blob/master/verify/CERTIFY_README.md 

# Installation #

- Add this repository in the composer.json file as a repository.
- Install with Composer by executing the following command:
```bash
$ composer require --prefer-source gudtech/retailops_magento2
```

# Release Notes #

## Release 2.1.0
- Improvement: Added functionality to work with regular inventory (Magent_CatalogInventory module) - [M2R-118]

## Release 2.0.0
- Improvement: Added Compatibility with latest Magento 2 release. 
- Improvement: Added missing endpoints. 
- Improvement: Cleanup code, created bugfixes and fixed spelling mistakes.
