<?php

namespace Gudtech\RetailOps\Model\Api\Map;

use Magento\Framework\App\ObjectManager;
use Gudtech\RetailOps\Service\CalculateDiscountInterface;
use Gudtech\RetailOps\Service\CalculateItemPriceInterface;
use Gudtech\RetailOps\Api\Order\Map\CalculateAmountInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

/**
 * Map order class.
 *
 */
class Order
{
    /**
     * RetailOps order status
     *
     * @var integer
     */
    const ORDER_STATUS_PENDING = 0;
    const ORDER_STATUS_PULLED = 1;

    /**
     * @var \Gudtech\RetailOps\Service\CalculateDiscountInterface
     */
    protected $calculateDiscount;

    /**
     * @var \Gudtech\RetailOps\Service\Order\Map\RewardPointsInterface
     */
    protected $rewardPoints;

    /**
     * Status for retailops
     * @var array $retailopsItemStatus
     * http://gudtech.github.io/retailops-sdk/v1/channel/#!/default/post_order_pull_v1
     */
    public static $retailopsItemStatus = ['ship', 'advisory', 'instore'];

    /**
     * @var array $paymentProcessingType
     * from http://gudtech.github.io/retailops-sdk/v1/channel/#!/default/post_order_pull_v1
     */
    public static $paymentProcessingType = [
        'default' => 'channel_payment',
        'reward' => 'channel_storecredit',
        'gift' => 'channel_giftcert',
        'authorized' => 'authorize.net'
    ];

    /**
     * @var \Gudtech\RetailOps\Api\Order\Map\CalculateAmountInterface
     */
    public $calculateAmount;

    /**
     * Holds the item totals.
     *
     * @var array
     */
    private $itemTotals = [];

    /**
     * Order map constructor.
     *
     * @param CalculateDiscountInterface $calculateDiscount
     * @param CalculateItemPriceInterface $calculateItemPrice
     * @param CalculateAmountInterface $calculateAmount
     */
    public function __construct(
        CalculateDiscountInterface $calculateDiscount,
        CalculateItemPriceInterface $calculateItemPrice,
        CalculateAmountInterface $calculateAmount
    ) {
        $this->calculateDiscount = $calculateDiscount;
        $this->calculateItemPrice = $calculateItemPrice;
        $this->calculateAmount = $calculateAmount;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface[] $orders
     * @return array
     */
    public function getOrders($orders)
    {
        if (count($orders)) {
            $prepareOrders = [];
            /**
             * @var \Magento\Sales\Api\Data\OrderInterface $order
             */
            foreach ($orders as $order) {
                $prepareOrders[] = $this->prepareOrder($order);
                $order->setData('retailops_send_status', self::ORDER_STATUS_PULLED);
                $order->addStatusToHistory($order->getStatus(), "Exported to RetailOps for processing");
                $order->save();
            }

            return $prepareOrders;
        }
        return [];
    }

    /**
     * @param OrderInterface $order
     * @return mixed
     */
    private function prepareOrder(OrderInterface $order)
    {
        // Reset item totals
        $this->itemTotals = ['tax' => 0, 'discount' => 0];

        $prepareOrder = [];
        $prepareOrder['channel_order_refnum'] = $order->getIncrementId();
        $prepareOrder['currency_code'] = $order->getOrderCurrencyCode();
        $prepareOrder['channel_date_created'] = (new \DateTime($order->getCreatedAt(), new \DateTimeZone('UTC')))
            ->format('c');
        $prepareOrder['billing_address'] = $this->getAddress($order, $order->getBillingAddress());
        $prepareOrder['shipping_address'] = $this->getAddress($order, $order->getShippingAddress());
        $prepareOrder['order_items'] = $this->getOrderItems($order);
        $prepareOrder['ship_service_code'] = $order->getShippingDescription();

        // Add gift message if available
        if ($order->getGiftMessageAvailable()) {
            $giftHelper = ObjectManager::getInstance()->get(\Magento\GiftMessage\Helper\Message::class);
            $message = $giftHelper->getGiftMessage($order->getGiftMessageId());
            $prepareOrder['gift_message'] = $message;
        }

        $prepareOrder['currency_values'] = $this->getCurrencyValues($order);
        $prepareOrder['payment_transactions'] = $this->getPaymentTransactions($order);
        $prepareOrder['customer_info'] = $this->getCustomerInfo($order);
        $prepareOrder['ip_address'] = $order->getRemoteIp();
        return $this->clearNullValues($prepareOrder);
    }

    private function getCurrencyValues($order)
    {
        $values = [];
        $values['shipping_amt'] = $this->calculateAmount->calculateShipping($order);
        $values['tax_amt'] = (float)$order->getBaseTaxAmount() - $this->itemTotals['tax'];
        $values['discount_amt'] = $this->calculateDiscount->calculate($order) - $this->itemTotals['discount'];
        return $values;
    }

    /**
     * @param OrderInterface $order
     * @param OrderAddressInterface $orderAddress
     * @return array
     */
    private function getAddress($order, $orderAddress)
    {
        $address = [];
        $address['first_name'] = $orderAddress->getFirstname();
        $address['last_name'] = $orderAddress->getLastname();
        $address['address1'] = is_array($orderAddress->getStreet()) ? $orderAddress->getStreet()[0] : $orderAddress->getStreet();
        if (is_array($orderAddress->getStreet()) && count($orderAddress->getStreet()) > 1) {
            $address['address2'] = $orderAddress->getStreet()[1];
        }
        $address['city'] = $orderAddress->getCity();
        $address['postal_code'] = $orderAddress->getPostcode();
        $address['state_match'] = $orderAddress->getRegion();
        $address['country_match'] = $orderAddress->getCountryId();
        $address['company'] = $orderAddress->getCompany();

        return $address;
    }

    /**
     * @param  \Magento\Sales\Model\Order $order
     * @return array
     */
    public function getOrderItems($order)
    {
        $items = [];
        $item = [];
        $orderItems = $order->getItems();
        foreach ($orderItems as $orderItem) {

            if ($orderItem->getParentItem()) {
                continue;
            }

            $item['channel_item_refnum'] = $orderItem->getId();
            $item['sku'] = $orderItem->getSku();
            $item['sku_description'] = $orderItem->getName();
            $item['item_type'] = $this->getItemType($orderItem);
            $item['currency_values'] = $this->getItemCurrencyValues($orderItem);
            $item['quantity'] = $this->getQuantity($orderItem);

            $items[] = $item;
        }
        return $items;
    }

    /**
     * @param OrderItemInterface $item
     * @return int
     */
    private function getQuantity($item)
    {
        if ($item->getParentItem()) {
            $item = $item->getParentItem();
        }

        $qty = $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();

        return (int)$qty;
    }

    /**
     * @param OrderItemInterface $item
     * @return string
     */
    private function getItemType($item)
    {
        // @todo after design shipping with retailops add logic for orders
        return 'ship';
    }

    /**
     * @param $item
     * @return array
     */
    public function getItemCurrencyValues($item)
    {
        $itemCurrency = [];
        if ($item->getParentItem()) {
            $item = $item->getParentItem();
        }

        // Update totals for the order
        $this->itemTotals['tax'] += (float)$item->getBaseTaxAmount();
        $this->itemTotals['discount'] += (float)$item->getBaseDiscountAmount();

        // Retrieve line item data
        $itemCurrency['unit_price'] = (float)$this->calculateItemPrice->getPrice($item);;
        $itemCurrency['unit_tax'] = (float)$this->calculateItemPrice->getTax($item);
        $itemCurrency['discount_amt'] = (float)$this->calculateItemPrice->getDiscount($item);

        return $itemCurrency;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    public function getPaymentTransactions($order)
    {
        $paymentMethod = $order->getPayment();
        $paymentTransactions = [];
        $payment = [];
        $storeCredit = [];

        $payment['payment_processing_type'] = self::$paymentProcessingType['default'];
        $payment['payment_type'] = $paymentMethod->getMethod();
        $payment['amount'] = $this->calculateAmount->calculateGrandTotal($order);
        $payment['transaction_type'] = 'charge';

        if ($order->getBaseCustomerBalanceAmount() > 0) {
            /**
             * Do not add store credit as separate payment as it's no supported by RetailOps to have multiple
             * transction_type with the same value.
             *
             * $storeCredit['payment_processing_type'] = self::$paymentProcessingType['reward'];
             * $storeCredit['payment_type'] = 'Store Credit';
             * $storeCredit['amount'] = (float)$order->getBaseCustomerBalanceAmount();
             * $storeCredit['transaction_type'] = 'charge';
             * $paymentTransactions[] = $storeCredit;
             */

            $payment['message'] = 'Store Credit used: '. $order->getBaseCustomerBalanceAmount();
            $payment['amount'] = $payment['amount'] + (float)$order->getBaseCustomerBalanceAmount();
        }

        $paymentTransactions[] = $payment;

        return $this->getGiftPaymentTransaction($paymentTransactions, $order);
    }

    /**
     * @param array $payments
     * @param  \Magento\Sales\Model\Order $order
     * @return array
     */
    public function getGiftPaymentTransaction(array $payments, $order)
    {
        if ($order->getGiftCardsAmount() > 0) {
            $paymentG = [];
            $paymentG['payment_type'] = 'gift';
            $paymentG['payment_processing_type'] = self::$paymentProcessingType['gift'];
            $paymentG['amount'] = (float)$order->getBaseGiftCardsAmount();
            $paymentG['transaction_type'] = 'giftcard';
            $payments[] = $paymentG;
        }

        return $payments;
    }

    /**
     * @param  \Magento\Sales\Model\Order $order
     * @return array
     */
    private function getCustomerInfo($order)
    {
        $customer = [];
        $customer['email_address'] = $order->getCustomerEmail();
        if ($order->getCustomerIsGuest()) {
            $customer['full_name'] = 'Guest';
        } else {
            $customer['full_name'] = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
        }

        return $customer;
    }

    /**
     * @param  array $orders
     * @return mixed
     */
    public function clearNullValues(&$orders)
    {
        foreach ($orders as $key => &$order) {
            if (is_array($order)) {
                $this->clearNullValues($order);
            }
            if ($order === null) {
                unset($orders[$key]);
            }
        }
        return $orders;
    }
}
