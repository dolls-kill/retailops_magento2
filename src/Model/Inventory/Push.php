<?php

namespace Gudtech\RetailOps\Model\Inventory;

use Gudtech\RetailOps\Model\InventoryHistoryFactory;
use Gudtech\RetailOps\Model\InventoryHistoryRepository;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\Stock;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Sales\Model\Order as MagentoOrder;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory as OrderItemCollectionFactory;
use Gudtech\RetailOps\Model\Catalog\Push as CatalogPush;
use Gudtech\RetailOps\Model\Logger\Monolog;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;

/**
 * Inventory push class.
 */
class Push
{
    /**
     * @var string
     */
    const CATALOG_INVENTORY_FACILITY_NAME = 'SB';

    /**
     * @var array
     */
    const RESERVED_QUANTITY_SOURCE_CODE = ['SB'];

    /**
     * @var string
     */
    private $status = 'success';

    /**
     * @var array
     */
    private $events = [];

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    /**
     * @var InventoryHistoryInterface
     */
    private $inventoryHistoryRepository;

    /**
     * @var InventoryHistoryFactory
     */
    private $inventoryHistoryFactory;

    /**
     * @var OrderItemCollectionFactory
     */
    private $orderItemCollectionFactory;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * Push constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param InventoryHistoryRepository $inventoryHistoryRepository
     * @param InventoryHistoryFactory $inventoryHistoryFactory
     * @param OrderItemCollectionFactory $orderItemCollectionFactory
     * @param StockRegistryInterface $stockRegistry
     * @param ModuleManager $moduleManager
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        InventoryHistoryRepository $inventoryHistoryRepository,
        InventoryHistoryFactory $inventoryHistoryFactory,
        OrderItemCollectionFactory $orderItemCollectionFactory,
        StockRegistryInterface $stockRegistry,
        Monolog $logger,
        ModuleManager $moduleManager
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->inventoryHistoryRepository = $inventoryHistoryRepository;
        $this->inventoryHistoryFactory = $inventoryHistoryFactory;
        $this->orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->stockRegistry = $stockRegistry;
        $this->logger = $logger;
    }

    /**
     * Processes the inventory data.
     *
     * @param array $inventory
     */
    public function processData($inventory)
    {
        try {
            // Make sure SKU exists, throws exception when product can't be found.
            $product = $this->productRepository->get($inventory['sku']);

            foreach ($inventory['quantity_detail'] as $sourceItemData) {

                $addToHistory = true;

                // Inventory update with regular Magento_CatalogInventory module.
                if ($sourceItemData['facility_name'] == self::CATALOG_INVENTORY_FACILITY_NAME) {
                    $stockItem = $this->stockRegistry->getStockItem($product->getId());

                    $currentQuantity = $stockItem->getQty();
                    $reservedQuantity = $this->getReservedQuantity($product->getSku());
                    $newQuantity = $sourceItemData['available_quantity'] - $reservedQuantity;

                    $stockItem->setQty($newQuantity);
                    if ($newQuantity > 0) {
                        $stockItem->setIsInStock(Stock::STOCK_IN_STOCK);
                    } else {
                        $stockItem->setIsInStock(Stock::STOCK_OUT_OF_STOCK);
                    }
                    $stockItem->save();

                } else {
                    $addToHistory = false;
                }

                if ($addToHistory) {
                    $this->addInventoryHistory(
                        $product,
                        $sourceItemData['facility_name'],
                        $sourceItemData,
                        $currentQuantity,
                        $reservedQuantity,
                        $newQuantity
                    );
                }
            }

            $retry=0;
            $this->logger->addInfo('Processing inventory for SKU: '.$product->getSku());
            while ($retry < CatalogPush::RETRY) {
                try {
                    $this->productRepository->save($product);
                    $this->logger->addInfo('Saving inventory success for Sku : '.$product->getSku());
                    break;
                } catch(CouldNotSaveException $exception) {
                    sleep(1);
                    $retry = $retry + 1;
                    $this->logger->addCritical("Saving inventory failed for SKU ". $product->getSku() .": ". $exception->getMessage());
                } catch (\Exception $exception) {
                    $this->logger->addCritical("Saving inventory failed for SKU ". $product->getSku() .": ". $exception->getMessage());
                    break;
                }
            }
            if ($retry >= CatalogPush::RETRY) {
                $this->logger->addCritical("Retry saving inventory failed for SKU ". $product->getSku());
            }

        } catch (Exception $exception) {
            $this->status = 'fail';
            $this->setEventsInfo($exception);
        } finally {
            $response = [];
            $response['status'] = $this->status;
            $response['events'] = $this->events;
            return $response;
        }
    }

    /**
     * @param Product $product
     * @param $sourceCode
     * @param $sourceItemData
     * @param $currentQuantity
     * @param $reservedQuantity
     * @param $newQuantity
     */
    private function addInventoryHistory(
        Product $product,
        $sourceCode,
        $sourceItemData,
        $currentQuantity,
        $reservedQuantity,
        $newQuantity
    ) {
        // Log inventory update change.
        $inventoryHistory = $this->inventoryHistoryFactory->create();
        $inventoryHistory->setProductId($product->getId());
        $inventoryHistory->setSourceCode($sourceCode);
        $inventoryHistory->setRetailopsQuantity($sourceItemData['available_quantity']);
        $inventoryHistory->setMagentoQuantity($currentQuantity);
        $inventoryHistory->setReservedQuantity($reservedQuantity);
        $inventoryHistory->setNewQuantity($newQuantity);
        $this->inventoryHistoryRepository->save($inventoryHistory);
    }

    /**
     * Returns true if Magento_Inventory is enabled, otherwise false.
     *
     * @return bool
     */
    private function isMagentoInventoryEnabled()
    {
        return $this->moduleManager->isEnabled('Magento_Inventory');
    }

    /**
     * Get Source Items Hash Table by SKU
     *
     * @param string $sku
     * @return SourceItemInterface[]
     */
    private function getCurrentSourceItemsMap(string $sku): array
    {
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        $searchCriteria = $searchCriteriaBuilder->addFilter(ProductInterface::SKU, $sku)->create();
        $sourceItems = $this->sourceItemRepository->getList($searchCriteria)->getItems();

        $sourceItemMap = [];
        if ($sourceItems) {
            foreach ($sourceItems as $sourceItem) {
                $sourceItemMap[$sourceItem->getSourceCode()] = $sourceItem;
            }
        }
        return $sourceItemMap;
    }

    /**
     * Retrieves the reserved product quantity for orders which are not synced yet to RetailOps.
     *
     * @param $sku
     * @return int
     */
    private function getReservedQuantity($sku)
    {
        $reservedQuantity = 0;

        /**
         * @var \Magento\Sales\Model\ResourceModel\Order\Item\Collection $collection
         */
        $collection = $this->orderItemCollectionFactory->create();
        $connection = $collection->getConnection();

        $collection->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS);
        $collection->getSelect()->columns([new \Zend_Db_Expr('SUM(main_table.qty_ordered) - SUM(main_table.qty_canceled) - SUM(main_table.qty_refunded) AS reserved_quantity')]
        );
        $collection->getSelect()->joinLeft(
            ['so' => $connection->getTableName('sales_order')],
            'so.entity_id = main_table.order_id',
            []
        );
        $collection->getSelect()->where(
            'so.retailops_send_status=?',
            \Gudtech\RetailOps\Model\Api\Map\Order::ORDER_STATUS_PENDING
        );
        $collection->getSelect()->where('main_table.sku = ?', $sku);
        $collection->getSelect()->where(
            'so.state NOT IN (?)',
            [MagentoOrder::STATE_CANCELED, MagentoOrder::STATE_CLOSED, MagentoOrder::STATE_COMPLETE]
        );
        $collection->getSelect()->where(
            'main_table.product_type = ?',
            \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE
        );
        $collection->getSelect()->group('main_table.sku');
        $collection->load();

        foreach ($collection as $item) {
            $reservedQuantity = $reservedQuantity + $item->getReservedQuantity;
        }

        return $reservedQuantity;
    }

    /**
     * @param $exception Exception
     */
    private function setEventsInfo($exception)
    {
        $event = [];
        $event['event_type'] = 'error';
        $event['code'] = (string)$exception->getCode();
        $event['message'] = $exception->getMessage();
        $event['diagnostic_data'] = $exception->getFile();
        $this->events[] = $event;
    }
}
