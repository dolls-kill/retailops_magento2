<?php

namespace Gudtech\RetailOps\Model\Catalog;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Area;
use Magento\Store\Model\App\Emulation;
use Gudtech\RetailOps\Model\Catalog;
use Gudtech\RetailOps\Model\Logger\Monolog;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;

class Push extends Catalog
{
    const RETRY = 4;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * Catalog push constructor.
     *
     * @param array $dataAdapters
     * @param ProductRepositoryInterface $productRepository
     * @param ProductFactory $productFactory
     */
    public function __construct(
        $dataAdapters = [],
        ProductRepositoryInterface $productRepository,
        ProductFactory $productFactory,
        Monolog $logger,
        Emulation $emulation
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->emulation = $emulation;
        $this->logger = $logger;

        parent::__construct($dataAdapters);
    }

    /**
     * @return $this
     */
    public function beforeDataPrepare()
    {
        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->beforeDataPrepare();
        }

        return $this;
    }

    /**
     * @param array $productData
     * @return $this
     */
    public function prepareData(array &$productData)
    {
        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->prepareData($productData);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function afterDataPrepare()
    {
        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->afterDataPrepare();
        }

        return $this;
    }

    /**
     * @param array $productData
     * @return $this
     */
    public function validateData(array $productData)
    {
        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->validateData($productData);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function beforeDataProcess()
    {
        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->beforeDataProcess();
        }

        return $this;
    }

    /**
     * @param array $productData
     * @return bool
     * @throws Exception
     */
    public function processData($productData)
    {
        $this->emulation->startEnvironmentEmulation(0, Area::AREA_ADMINHTML);

        $product = $this->productFactory->create();

        if ($productId = $product->getIdBySku($productData['General']['SKU'])) {
            $product = $this->productRepository->getById($productId);
        }

        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->processData($productData, $product);
        }

        if ($product->getOptions()) {
            $product->getOptionInstance()->unsetOptions();
        }

        $retry=0;
        while ($retry < self::RETRY) {
            try {
                $this->productRepository->save($product);
                $this->logger->addInfo('Saving data success for Sku : '.$product->getSku());
                break;
            } catch(CouldNotSaveException $exception) {
                sleep(1);
                $retry = $retry + 1;
                $this->logger->addCritical("Saving data failed for SKU ". $product->getSku() .": ". $exception->getMessage());
            } catch (\Exception $exception) {
                $this->logger->addCritical("Saving data failed for SKU ". $product->getSku() .": ". $exception->getMessage());
                break;
            }
        }
        if ($retry >= self::RETRY) {
            $this->logger->addCritical("Retry saving data failed for SKU ". $product->getSku());
        }

        $this->emulation->stopEnvironmentEmulation();

        return true;
    }

    /**
     * @return $this
     */
    public function afterDataProcess()
    {
        foreach ($this->getDataAdapters() as $dataAdapter) {
            $dataAdapter->afterDataProcess();
        }
        return $this;
    }
}