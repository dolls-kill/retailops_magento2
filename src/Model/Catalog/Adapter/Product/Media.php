<?php

namespace Gudtech\RetailOps\Model\Catalog\Adapter\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Gallery\Processor as ProductMediaGalleryProcessor;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\RuntimeException;
use Magento\Framework\Filesystem\DirectoryList;
use Gudtech\RetailOps\Model\Catalog\Adapter;
use Magento\Framework\HTTP\ClientFactory as HttpClientFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Store\Model\StoreManagerInterface;
use Gudtech\RetailOps\Model\Logger\Monolog;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;
use Gudtech\RetailOps\Model\Catalog\Push;

class Media extends Adapter
{
    private $mediaDataToSave = [];

    private $productRepository;
    private $productMediaGalleryProcessor;
    private $directoryList;
    private $httpClientFactory;
    private $fileSystem;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Media constructor.
     *
     * @param ProductRepository $productRepository
     * @param ProductMediaGalleryProcessor $productMediaGalleryProcessor
     * @param DirectoryList $directoryList
     * @param HttpClientFactory $httpClientFactory
     * @param File $fileSystem
     */
    public function __construct(
        ProductRepository $productRepository,
        ProductMediaGalleryProcessor $productMediaGalleryProcessor,
        DirectoryList $directoryList,
        Monolog $logger,
        HttpClientFactory $httpClientFactory,
        File $fileSystem,
        StoreManagerInterface $storeManager
    ) {
        $this->productRepository = $productRepository;
        $this->productMediaGalleryProcessor = $productMediaGalleryProcessor;
        $this->directoryList = $directoryList;
        $this->httpClientFactory = $httpClientFactory;
        $this->fileSystem = $fileSystem;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * @param array $productData
     * @param Product $product
     * @return mixed|void
     */
    public function processData(array $productData, Product $product)
    {
        if ($product->getId() && (!isset($productData['unset_other_media']) || $productData['unset_other_media'])) {
            $this->clearProductGallery($product);
        }

        if (isset($productData['Images'])) {
            $this->mediaDataToSave[$productData['General']['SKU']] = json_encode($productData['Images']);
        }

        if (isset($productData['straight_media_process']) && $productData['straight_media_process']) {
            $this->straightMediaProcessing = true;
        }
    }

    /**
     * @param array $mediaDataToSave
     * @return $this|void
     */
    public function afterDataProcess()
    {
        if ($this->mediaDataToSave) {
            foreach ($this->mediaDataToSave as $sku => $data) {
                $retry=0;
                $this->logger->addInfo('Processing image for SKU: '.$sku);
                while ($retry < Push::RETRY) {
                    try {
                        $this->setProductGallery($sku, $data);
                        $this->logger->addInfo('Saving image success for Sku : '.$sku);
                        break;
                    } catch(CouldNotSaveException $exception) {
                        sleep(1);
                        $retry = $retry + 1;
                        $this->logger->addCritical("Saving image failed for SKU ". $sku .": ". $exception->getMessage());
                    } catch (\Exception $exception) {
                        $this->logger->addCritical("Saving image failed for SKU ". $sku .": ". $exception->getMessage());
                        break;
                    }
                }
                if ($retry >= Push::RETRY) {
                    $this->logger->addCritical("Retry saving image failed for SKU ". $sku);
                }
            }
        }
    }

    /**
     * Unset product image gallery
     *
     * @param Product $product
     */
    public function clearProductGallery(Product $product)
    {
        $mediaGalleryItems = $product->getMediaGalleryEntries();

        foreach ($mediaGalleryItems as $key => $item) {
            $this->productMediaGalleryProcessor->removeImage($product, $item->getFile());
            unset($mediaGalleryItems[$key]);
        }

        $product->setMediaGalleryEntries($mediaGalleryItems);
    }

    /**
     * Set products media
     *
     * @param string $sku
     * @param array $data
     * @return $this|void
     */
    public function setProductGallery($sku, $data)
    {
        $data = json_decode($data, true);

        $product = $this->productRepository->get($sku);

        foreach ($data as $productImage) {

            if ($productImage['Image']['Url'] !== '') {

                $file = $this->productImageExists($product, $productImage);
                if (!$file) {
                    $imageUrl = $productImage['Image']['Url'];

                    $fileName = $this->getFileName($imageUrl);

                    if (isset($productImage['Image']['Types'])) {
                        $mediaTypes = preg_replace('/\s+/', '', $productImage['Image']['Types']);
                        $mediaAttribute = explode(",", $mediaTypes);
                        $product->addImageToMediaGallery(
                            $fileName,
                            $mediaAttribute,
                            true,
                            false
                        );
                    } else {
                        $product->addImageToMediaGallery(
                            $fileName,
                            null,
                            true,
                            false
                        );
                    }
                }
            }
        }
        //Set image attribute labels to all store views
        $this->storeManager->setCurrentStore(0);

        $this->productRepository->save($product);
    }

    /**
     * Get existing image filename, if any, based on mediakey and filename
     *
     * @param Product $product
     * @param string $productImage
     * @return boolean
     * @throws LocalizedException
     */
    private function productImageExists($product, $productImage)
    {
        $mediaGalleryItems = $product->getMediaGalleryEntries();

        foreach ($mediaGalleryItems as $key => $item) {
            if ($this->getFileName($item->getFile()) == $this->getFileName($productImage['Image']['Url'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the filename from a full path.
     *
     * @param string $path
     * @return string
     */
    private function getFileName($path)
    {
        $pathItems = pathinfo($path);

        return $pathItems['basename'];
    }
}
