<?php

namespace Gudtech\RetailOps\Model\Catalog\Adapter\Product\Type;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory as OptionsFactory;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Model\Entity;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set as AttributeSet;
use Gudtech\RetailOps\Model\Catalog\Adapter;
use Gudtech\RetailOps\Model\Catalog\Adapter\Product\Attribute as AttributeAdapter;
use Gudtech\RetailOps\Model\Catalog\Push;
use Gudtech\RetailOps\Model\Logger\Monolog;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;

class Configurable extends Adapter
{
    /**
     * @var string
     */
    const CONFIGURABLE_ATTRIBUTE_NAME = 'Configurable SKU';

    /**
     * @var array
     */
    private $associations  = [];

    /**
     * @var array
     */
    private $configurableProducts = [];

    /**
     * @var array
     */
    private $configurableAttributes = [];

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var array
     */
    private $configurableProductSku = [];

    /**
     * @var AttributeAdapter
     */
    private $attributeAdapter;

    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var OptionsFactory
     */
    private $optionsFactory;

    /**
     * @var Entity
     */
    private $eavEntity;

    /**
     * @var AttributeSet
     */
    private $attributeSet;

    /**
     * Configurable product constructor.
     *
     * @param ProductRepository $productRepository
     * @param ProductFactory $productFactory
     * @param OptionsFactory $optionsFactory
     * @param Entity $eavEntity
     * @param AttributeAdapter $attributeAdapter
     * @param AttributeRepository $attributeRepository
     * @param AttributeSet $attributeSet

     */
    public function __construct(
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        OptionsFactory $optionsFactory,
        Entity $eavEntity,
        AttributeAdapter $attributeAdapter,
        AttributeRepository $attributeRepository,
        Monolog $logger,
        AttributeSet $attributeSet
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->optionsFactory = $optionsFactory;
        $this->eavEntity = $eavEntity;
        $this->attributeAdapter = $attributeAdapter;
        $this->attributeRepository = $attributeRepository;
        $this->attributeSet = $attributeSet;
        $this->logger = $logger;
    }

    /**
     * @param array $productData
     * @param Product $product
     * @return mixed|void
     */
    public function processData(array $productData, Product $product)
    {
        $sku = $productData['General']['SKU'];

        if (isset($productData['Configurable Attributes'])) {
            $this->configurableProducts[$sku] = $product;
            $this->configurableProductSku[] = $sku;
            $this->configurableAttributes[$sku] = [];

            $configurableAttributes = explode(",", $productData['Configurable Attributes']);

            foreach($configurableAttributes as $configurableAttribute) {
                $this->configurableAttributes[$sku][] = trim($configurableAttribute);
            }
        } else {
            $configurableSku = $this->getConfigurableSku($productData);

            if ($configurableSku) {
                if (!isset($this->associations[$configurableSku])) {
                    $this->associations[$configurableSku] = [];
                }
                $this->associations[$configurableSku][] = $sku;
            }
        }
    }

    /**
     *
     * @return $this|void
     */
    public function afterDataProcess()
    {
        foreach($this->configurableProducts as $configurableProduct)
        {
            $configurableAttributeIds = [];

            foreach ($this->configurableAttributes[$configurableProduct->getSku()] as $attributeCode) {
                $attribute = $this->attributeRepository->get($this->getEntityTypeId(), $attributeCode);
                $configurableAttributeIds[] = $attribute->getAttributeId();
            }

            if ($configurableAttributeIds) {

                $configurableProduct->getTypeInstance()->setUsedProductAttributeIds(
                    $configurableAttributeIds,
                    $configurableProduct
                );

                $configurableAttributesData = $configurableProduct->getTypeInstance()->getConfigurableAttributesAsArray(
                    $configurableProduct
                );

                $simpleProductIds = [];

                foreach ($this->associations[$configurableProduct->getSku()] as $simpleProductSku) {
                    $simpleProduct = $this->productRepository->get($simpleProductSku);
                    $simpleProductIds[] = $simpleProduct->getId();

                    foreach ($this->configurableAttributes[$configurableProduct->getSku()] as $attributeCode) {
                        $attribute = $this->attributeRepository->get($this->getEntityTypeId(), $attributeCode);

                        $configurableAttributesData[$attribute->getAttributeId()]['values'][] = [
                            'label' => $attribute->getDefaultFrontendLabel(),
                            'attribute_id' => $attribute->getId(),
                            'value_index' => $simpleProduct->getData($attribute->getAttributeCode()),
                        ];
                    }
                }

                $configurableOptions = $this->optionsFactory->create($configurableAttributesData);
                $configurableProduct->setConfigurableAttributesData($configurableAttributesData);

                $extensionConfigurableAttributes = $configurableProduct->getExtensionAttributes();
                $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
                $extensionConfigurableAttributes->setConfigurableProductLinks($simpleProductIds);
                $configurableProduct->setExtensionAttributes($extensionConfigurableAttributes);

                $configurableProduct->setCanSaveConfigurableAttributes(true);

                $retry=0;
                while ($retry < Push::RETRY) {
                    try {
                        $this->productRepository->save($configurableProduct);
                        $this->logger->addInfo('Saving attributes success for Sku : '.$configurableProduct->getSku());
                        break;
                    } catch(CouldNotSaveException $exception) {
                        sleep(1);
                        $retry = $retry + 1;
                        $this->logger->addCritical("Saving attributes failed for SKU ". $configurableProduct->getSku() .": ". $exception->getMessage());
                    } catch (\Exception $exception) {
                        $this->logger->addCritical("Saving attributes failed for SKU ". $configurableProduct->getSku() .": ". $exception->getMessage());
                        break;
                    }
                }
                if ($retry >= Push::RETRY) {
                    $this->logger->addCritical("Retry saving attributes failed for SKU ". $configurableProduct->getSku());
                }
            }
        }
    }

    /**
     * Returns the entity type id.
     *
     * @return int
     */
    private function getEntityTypeId()
    {
        return $this->eavEntity->setType(Product::ENTITY)->getTypeId();
    }

    /**
     * @param $productData
     * @return string
     */
    private function getConfigurableSku($productData)
    {
        $configurableSku = '';

        foreach($productData['Additional Attributes']['Attributes'] as $attribute) {
            if ($attribute['Attribute']['Name'] == self::CONFIGURABLE_ATTRIBUTE_NAME) {
                $configurableSku = trim($attribute['Attribute']['Value']);
            }
        }

        return $configurableSku;
    }
}
