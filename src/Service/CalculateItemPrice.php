<?php

namespace Gudtech\RetailOps\Service;

use Magento\Sales\Api\Data\OrderItemInterface;

/**
 * Calculate item price class.
 *
 */
class CalculateItemPrice implements CalculateItemPriceInterface
{
    /**
     * Returns the item price.
     *
     * @param OrderItemInterface $item
     * @return float
     */
    public function getPrice(OrderItemInterface $item):float
    {
        return (float)$item->getBasePrice();
    }

    /**
     * Returns the tax per item.
     *
     * @param OrderItemInterface $item
     * @return float
     */
    public function getTax(OrderItemInterface $item):float
    {
        $qty = (float)$item->getQtyOrdered();
        $tax = (float)$item->getTaxAmount();
        return round($tax/$qty, 4, PHP_ROUND_HALF_UP);
    }

    /**
     * Returns the discount per item.
     *
     * @param OrderItemInterface $item
     * @return float
     */
    public function getDiscount(OrderItemInterface $item):float
    {
        $qty = (float)$item->getQtyOrdered();
        $discount = (float)$item->getDiscountAmount();
        return round($discount/$qty, 4, PHP_ROUND_HALF_UP);
    }
}
