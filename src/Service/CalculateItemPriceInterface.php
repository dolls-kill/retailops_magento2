<?php

namespace Gudtech\RetailOps\Service;

use Magento\Sales\Api\Data\OrderItemInterface;

/**
 * Calculate item price interface.
 *
 */
interface CalculateItemPriceInterface
{
    /**
     * Returns the item price
     *
     * @param OrderItemInterface $item
     * @return float
     */
    public function getPrice(OrderItemInterface $item);

    /**
     * Returns the tax amount per item.
     *
     * @param OrderItemInterface $item
     * @return float
     */
    public function getTax(OrderItemInterface $item);

    /**
     * Returns the discount amount per item.
     *
     * @param OrderItemInterface $item
     * @return float
     */
    public function getDiscount(OrderItemInterface $item);

}
